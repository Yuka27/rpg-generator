import React from 'react';
import PropTypes from 'prop-types';

import CircularProgress from '@material-ui/core/CircularProgress';

import HeroListItem from './HeroListItem';

const HeroList = ({heroes, isLoading}) => (
    <div className="hero-container">
        {isLoading ? <CircularProgress /> : heroes.map(hero => <HeroListItem key={`hero${hero.id}`} hero={hero} />)}
    </div>
);

HeroList.defaultProps = {
    heroes: [],
    isLoading: false
};

HeroList.propTypes = {
    heroes: PropTypes.array,
    isLoading: PropTypes.bool
};

export default HeroList;
