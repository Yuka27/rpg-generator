import React from 'react';
import PropTypes from 'prop-types';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import LabelImportant from '@material-ui/icons/LabelImportant';
import Favorite from '@material-ui/icons/Favorite';

import labels from '../labels'

const renderItem = (label) => (
    <ListItem>
        <ListItemIcon>
            <LabelImportant />
        </ListItemIcon>
        <ListItemText primary={label} />
    </ListItem>
);

const HeroItemDetails = ({hero}) => (
    <List component="nav" aria-label="main mailbox folders">
        <ListItem>
            <ListItemIcon>
                <Favorite color='error' />
            </ListItemIcon>
            <ListItemText primary={`${hero.hp} / ${hero.hp}`} />
        </ListItem>
        {renderItem(`${labels.melee}: ${hero.skills.melee}`)}
        {renderItem(`${labels.oneHandedWeapon}: ${hero.skills.oneHandedWeapon}`)}
        {renderItem(`${labels.twoHandedWeapon}: ${hero.skills.twoHandedWeapon}`)}
        {renderItem(`${labels.thrownWeapon}: ${hero.skills.thrownWeapon}`)}
        {renderItem(`${labels.defense}: ${hero.skills.defense}`)}
    </List>
);

HeroItemDetails.propTypes = {
    hero: PropTypes.shape({
        hp: PropTypes.number,
        skills: PropTypes.shape({
            melee: PropTypes.number,
            oneHandedWeapon: PropTypes.number,
            twoHandedWeapon: PropTypes.number,
            thrownWeapon: PropTypes.number,
            defense: PropTypes.number,
        })
    }).isRequired
};

export default HeroItemDetails;
