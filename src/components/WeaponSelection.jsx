import React from 'react';
import PropTypes from 'prop-types';

import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import labels from '../labels';

const WeaponSelection = ({weapons, selected}) => (
    <FormControl variant="outlined">
        <Select value={selected} >
            <MenuItem value={-1}>{labels.noneWeapon}</MenuItem>
            {weapons.map(weapon => <MenuItem key={`weapon${weapon.id}`} value={weapon.id}>{weapon.name}</MenuItem>)}
        </Select>
    </FormControl>
);

WeaponSelection.defaultProps = {
    selected: -1
};

WeaponSelection.propTypes = {
    weapons: PropTypes.array.isRequired,
    selected: PropTypes.number
};

export default WeaponSelection;
