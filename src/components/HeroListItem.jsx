import React from 'react';
import PropTypes from 'prop-types';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import IconButton from '@material-ui/core/IconButton';

import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import HeroItemDetails from './HeroItemDetails';

const HeroListItem = ({hero}) => (
    <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <FormControlLabel
                onClick={event => event.stopPropagation()}
                onFocus={event => event.stopPropagation()}
                control={<Checkbox />}
                label={<Typography>{hero.name}</Typography>}
            />
            <FormControlLabel
                onClick={event => event.stopPropagation()}
                onFocus={event => event.stopPropagation()}
                control={
                    <IconButton size="small">
                        <EditIcon fontSize="inherit" />
                    </IconButton>
                }
            />
            <FormControlLabel
                onClick={event => event.stopPropagation()}
                onFocus={event => event.stopPropagation()}
                control={
                    <IconButton size="small">
                        <DeleteIcon fontSize="inherit" />
                    </IconButton>
                }
            />
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
            <HeroItemDetails hero={hero} />
        </ExpansionPanelDetails>
    </ExpansionPanel>
);

HeroListItem.propTypes = {
    hero: PropTypes.shape({
        name: PropTypes.string,
    }).isRequired
};

export default HeroListItem;
