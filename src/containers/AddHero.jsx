import React, {Component} from 'react';
import PropTypes from 'prop-types';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import CircularProgress from '@material-ui/core/CircularProgress';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import LabelImportant from '@material-ui/icons/LabelImportant';
import Favorite from '@material-ui/icons/Favorite';
import Face from '@material-ui/icons/Face';

import WeaponSelection from '../components/WeaponSelection';
import {getWeapons} from "../actions";
import {statusType} from "../constants";
import labels from '../labels';
import {connect} from "react-redux";

import './AddHero.scss';

const renderInputItem = (label, icon) => (
    <ListItem>
        <ListItemIcon>
            {icon || <LabelImportant />}
        </ListItemIcon>
        <TextField
            label={label}
            variant="outlined"
        />
    </ListItem>
);

const renderSelectionItem = (weapons, selected, isSecondary) => (
    <ListItem>
        <ListItemIcon>
            <LabelImportant color={isSecondary ? 'secondary' : 'inherit'} />
        </ListItemIcon>
        <WeaponSelection selected={selected} weapons={weapons}/>
    </ListItem>
)

class AddHero extends Component {
    componentDidMount() {
        this.props.getWeapons()
    }

    render() {
        const {onCancel, weapons, isOpened, isLoading} = this.props;
        return(
            <Dialog onClose={onCancel} open={isOpened} maxWidth="xl" fullWidth>
                <DialogTitle id="simple-dialog-title">{labels.createHeroHeader}</DialogTitle>
                <div className='add-hero-form'>
                    <List component="nav" className="form-container">
                        <div className="form-column">
                            <div className="content">
                                <h3>{labels.heroDetailsHeader}</h3>
                                {renderInputItem(labels.name, <Face color='secondary' />)}
                                {renderInputItem(labels.hp, <Favorite color='error' />)}
                                {renderInputItem(labels.defense)}
                            </div>
                        </div>
                        <div className="form-column">
                            <div className="content">
                                <h3>{labels.combatStatsHeader}</h3>
                                {renderInputItem(labels.melee)}
                                {renderInputItem(labels.oneHandedWeapon)}
                                {renderInputItem(labels.twoHandedWeapon)}
                                {renderInputItem(labels.thrownWeapon)}
                            </div>
                        </div>
                        <div className='form-column'>
                            <div className="content">
                                <h3>{labels.weaponSelectionHeader}</h3>
                                {isLoading
                                    ? <CircularProgress />
                                    : <div>
                                        {renderSelectionItem(weapons, 5, true)}
                                        {renderSelectionItem(weapons, 20, )}
                                        {renderSelectionItem(weapons)}
                                        {renderSelectionItem(weapons)}
                                        {renderSelectionItem(weapons)}
                                    </div>
                                }
                            </div>
                        </div>
                    </List>
                    <ButtonGroup color="primary">
                        <Button onClick={onCancel}>Cancel</Button>
                        <Button variant="contained">Add</Button>
                    </ButtonGroup>
                </div>
            </Dialog>
        );
    }
}

const mapStateToProps = (state) => ({
    weapons: state.weapons.data,
    isLoading: state.weapons.status === statusType.loading
});

const mapDispatchToProps = (dispatch) => ({
    getWeapons: () => dispatch(getWeapons())
});

AddHero.defaultProps = {
    isLoading: false,
    weapons: []
};

AddHero.propTypes = {
    getWeapons: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    weapons: PropTypes.array,
    isLoading: PropTypes.bool
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddHero);
