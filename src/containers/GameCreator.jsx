import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import {getHeroes} from '../actions';
import {statusType} from '../constants';
import labels from '../labels';

import HeroList from '../components/HeroList';
import AddHero from './AddHero';
import './GameCreator.scss'

class GameCreator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFormVisible: false
        };
    }

    componentDidMount() {
        this.props.getHeroes();
    }

    openForm = () => {this.setState({isFormVisible: true})}
    closeForm = () => {this.setState({isFormVisible: false})}

    render() {
        const {isFormVisible} = this.state;
        const {heroes, isLoading} = this.props;
        return (
            <div className="creator-container">
                <div className="form-column">
                    <div className="subtitle">
                        <Fab color="secondary" size="small" onClick={this.openForm}>
                            <AddIcon />
                        </Fab>
                        <h2>{labels.availableHeroesHeader}</h2>
                    </div>
                    <AddHero onCancel={this.closeForm} isOpened={isFormVisible} />
                    <HeroList isLoading={isLoading} heroes={heroes} />
                </div>
                <div className="form-column">
                    <h2>{labels.selectedHeroesHeader}</h2>
                    <p>Todo: Selected Heroes</p>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => ({
    heroes: state.heroes.data,
    isLoading: state.heroes.status === statusType.loading
});

const mapDispatchToProps = (dispatch) => ({
    getHeroes: () => dispatch(getHeroes())
});

GameCreator.defaultProps = {
    heroes: [],
    isLoading: false
};

GameCreator.propTypes = {
    heroes: PropTypes.array,
    isLoading: PropTypes.bool,
    getHeroes: PropTypes.func.isRequired
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GameCreator);
