export const GET_HEROES = 'GET_HEROES';
export const GET_HEROES_SUCCESS = 'GET_HEROES_SUCCESS';
export const GET_HEROES_ERROR = 'GET_HEROES_ERROR';

export const GET_WEAPONS = 'GET_WEAPONS';
export const GET_WEAPONS_SUCCESS = 'GET_WEAPONS_SUCCESS';
export const GET_WEAPONS_ERROR = 'GET_WEAPONS_ERROR';
