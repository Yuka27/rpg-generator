import {api} from '../enviroments/config';
import {
    GET_HEROES,
    GET_HEROES_ERROR,
    GET_HEROES_SUCCESS,
    GET_WEAPONS,
    GET_WEAPONS_ERROR,
    GET_WEAPONS_SUCCESS
} from './actionTypes';

const urls = {
    heroes: `${api}/heroes`,
    weapons: `${api}/weapons`
};

const getActionCreator = (url, type, errorType, successType) => {
  return function(dispatch) {
    dispatch({type});

    fetch(url)
        .then(response => response.json())
        .then(data => dispatch({
          type: successType,
          payload: data
        }))
        .catch(error => dispatch({
              type: errorType,
              payload: error
            })
        );
  }
};

export const getHeroes = () => getActionCreator(urls.heroes, GET_HEROES, GET_HEROES_ERROR, GET_HEROES_SUCCESS);
export const getWeapons = () => getActionCreator(urls.weapons, GET_WEAPONS, GET_WEAPONS_ERROR, GET_WEAPONS_SUCCESS);
