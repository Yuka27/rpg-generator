import {GET_WEAPONS_SUCCESS, GET_WEAPONS_ERROR, GET_WEAPONS} from '../actions/actionTypes';
import {statusType} from '../constants';

const initialState = {
    data: [],
    status: ''
};

const weapons = (state = initialState, action) => {
    switch (action.type) {
        case GET_WEAPONS: {
            return {
                ...state,
                data: [],
                status: statusType.loading
            };
        }
        case GET_WEAPONS_SUCCESS: {
            return {
                ...state,
                data: action.payload,
                status: statusType.success
            };
        }
        case GET_WEAPONS_ERROR: {
            return {
                ...state,
                data: [],
                status: statusType.error
            };
        }
        default:
            return state;
    }
};

export default weapons;
