import {GET_WEAPONS_SUCCESS, GET_WEAPONS_ERROR, GET_WEAPONS} from '../actions/actionTypes';
import {statusType} from '../constants';
import weapons from './weapons'

describe('weapons reducer', () => {
  it('should handle initial state', () => {
    expect(
      weapons(undefined, {})
    ).toEqual({
      data: [],
      status: ''
    })
  });

  it('should handle GET_WEAPONS', () => {
    expect(
      weapons([], {
        type: GET_WEAPONS,
        payload: []
      })
    ).toEqual({
        data: [],
        status: statusType.loading
      })
  });

  it('should handle GET_WEAPONS_ERROR', () => {
    expect(
      weapons([], {
        type: GET_WEAPONS_ERROR,
        payload: []
      })
    ).toEqual({
        data: [],
        status: statusType.error
      })
  });

  it('should handle GET_WEAPONS_SUCCESS', () => {
    const result = [
        {
            id: 0,
            name: "gołe pięści",
            category: "A",
            maxHp: 15,
            comment:"obdarte kostki Q_Q",
        }
    ];

    expect(
      weapons([], {
        type: GET_WEAPONS_SUCCESS,
        payload: result
      })
    ).toEqual({
        data: result,
        status: statusType.success
      })
  });
});
