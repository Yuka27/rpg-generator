import {GET_HEROES_ERROR, GET_HEROES_SUCCESS, GET_HEROES} from '../actions/actionTypes';
import {statusType} from '../constants';
import heroes from './heroes'

describe('heroes reducer', () => {
  it('should handle initial state', () => {
    expect(
      heroes(undefined, {})
    ).toEqual({
      data: [],
      status: ''
    })
  });

  it('should handle GET_HEROES', () => {
    expect(
      heroes([], {
        type: GET_HEROES,
        payload: []
      })
    ).toEqual({
        data: [],
        status: statusType.loading
      })
  });

  it('should handle GET_HEROES_ERROR', () => {
    expect(
      heroes([], {
        type: GET_HEROES_ERROR,
        payload: []
      })
    ).toEqual({
        data: [],
        status: statusType.error
      })
  });

  it('should handle GET_HEROES_SUCCESS', () => {
    const result = [
      {
        id: 0,
        name: 'Inori',
        hp: 28,
        skills: {
          melee: 9,
          oneHandedWeapon: 9,
          twoHandedWeapon: 6,
          thrownWeapon: 6,
          defense: 6,
        },
        weapons: []
      },
    ];

    expect(
      heroes([], {
        type: GET_HEROES_SUCCESS,
        payload: result
      })
    ).toEqual({
        data: result,
        status: statusType.success
      })
  });
});
