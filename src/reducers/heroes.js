import {GET_HEROES, GET_HEROES_SUCCESS, GET_HEROES_ERROR} from '../actions/actionTypes';
import {statusType} from '../constants';

const initialState = {
    data: [],
    status: ''
};

const heroes = (state = initialState, action) => {
    switch (action.type) {
        case GET_HEROES: {
            return {
                ...state,
                data: [],
                status: statusType.loading
            };
        }
        case GET_HEROES_SUCCESS: {
            return {
                ...state,
                data: action.payload,
                status: statusType.success
            };
        }
        case GET_HEROES_ERROR: {
            return {
                ...state,
                data: [],
                status: statusType.error
            };
        }
        default:
            return state;
    }
};

export default heroes;
