import {combineReducers} from 'redux'
import heroes from './heroes';
import weapons from './weapons';

export default combineReducers({
  heroes,
  weapons
});
