module.exports = {
    oneHandedWeapon: 'Broń jednoręczna',
    twoHandedWeapon: 'Broń dwuręczna',
    melee: 'Walka wręcz',
    thrownWeapon: 'Broń miotana',
    defense: 'Obrona',
    noneWeapon: 'Brak broni',
    name: 'Imię',
    hp: 'HP',
    selectedHeroesHeader: 'Wybrane postacie',
    availableHeroesHeader: 'Dostępne postacie',
    heroDetailsHeader: 'Informacje ogólne',
    createHeroHeader: 'Dodaj postać',
    weaponSelectionHeader: 'Wybór broni',
    combatStatsHeader: 'Statystyki bitewne',
    appTitle: 'RPG generator',
    creatorTitle: 'Creator',
    gameTitle: 'Game'
};
