import {createMuiTheme} from '@material-ui/core/styles';

export const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#ef6c00'
        },
        secondary: {
            main: '#fb8c00'
        },
        type: 'dark'
    },
    status: {
        danger: 'orange',
    },
});
