import React, {Component} from 'react';

import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import SportsEsports from '@material-ui/icons/SportsEsports';
import GroupAdd from '@material-ui/icons/GroupAdd';

import GameCreator from './containers/GameCreator';
import labels from './labels';

import './assets/styles.scss';

const TabPanel = (props) => {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={ value !== index }
            id={ `nav-tabpanel-${index}` }
            {...other}
        >
            <Box>{children}</Box>
        </Typography>
    );
}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabValue: 0
        };
    }

    handleChange = (event, newValue) => {
        this.setState({tabValue: newValue});
    };

    render() {
        const {tabValue} = this.state;

        return (
            <div className="app">
                <h1>{labels.appTitle}</h1>
                <Paper square>
                    <Tabs
                        value={tabValue}
                        onChange={this.handleChange}
                        variant="fullWidth"
                        indicatorColor="secondary"
                        textColor="secondary"
                    >
                        <Tab icon={<GroupAdd />} label={labels.creatorTitle} />
                        <Tab icon={<SportsEsports />} label={labels.gameTitle} />
                    </Tabs>
                    <TabPanel className="container" value={tabValue} index={0}>
                        <GameCreator />
                    </TabPanel>
                    <TabPanel className="container" value={tabValue} index={1}>
                        <p>Todo: Game form</p>
                    </TabPanel>
                </Paper>
            </div>
        );
    }
}

export default App;
