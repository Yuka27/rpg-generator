module.exports = [
    {
        id: 0,
        name: "Inori",
        hp: 28,
        skills: {
            melee: 9,
            oneHandedWeapon: 9,
            twoHandedWeapon: 6,
            thrownWeapon: 6,
            defense: 6,
        },
        weapons: [
            {
                id: 0,
                weaponId: 16,
                name: "sztylet zwykły",
                category: "B",
                maxHp: 23,
                comment:"gdy trzeba sprawy załatwić po cichu",
                isMainWeapon: true,
            },
        ]
    },
    {
        id: 1,
        name: "Bran",
        hp: 27,
        skills: {
            melee: 6,
            oneHandedWeapon: 6,
            twoHandedWeapon: 6,
            thrownWeapon: 6,
            defense: 6,
        },
        weapons: [
            {
                id: 0,
                weaponId: 21,
                name: "kusza 2 ręczna",
                category: "C",
                maxHp: 26,
                comment:"ze strzałą wybuchającą +5 obr.",
                isMainWeapon: true,
            },
            {
                id: 1,
                weaponId: 14,
                name: "rapier",
                category: "B",
                maxHp: 24,
                comment:"rapierrrrr.... dźgaj ostrym końcem",
                isMainWeapon: false,
            },
        ]
    },
    {
        id: 2,
        name: "Riden",
        hp: 30,
        skills: {
            melee: 6,
            oneHandedWeapon: 6,
            twoHandedWeapon: 6,
            thrownWeapon: 6,
            defense: 6,
        },
        weapons: [
            {
                id: 0,
                weaponId: 21,
                name: "kusza 2 ręczna",
                category: "C",
                maxHp: 26,
                comment:"ze strzałą wybuchającą +5 obr.",
                isMainWeapon: true,
            },
            {
                id: 1,
                weaponId: 14,
                name: "rapier",
                category: "B",
                maxHp: 24,
                comment:"rapierrrrr.... dźgaj ostrym końcem",
                isMainWeapon: false,
            },
        ]
    },
    {
        id: 3,
        name: "Solis",
        hp: 27,
        skills: {
            melee: 6,
            oneHandedWeapon: 6,
            twoHandedWeapon: 6,
            thrownWeapon: 6,
            defense: 6,
        },
        weapons: [
            {
                id: 0,
                weaponId: 21,
                name: "kusza 2 ręczna",
                category: "C",
                maxHp: 26,
                comment:"ze strzałą wybuchającą +5 obr.",
                isMainWeapon: true,
            },
            {
                id: 1,
                weaponId: 14,
                name: "rapier",
                category: "B",
                maxHp: 24,
                comment:"rapierrrrr.... dźgaj ostrym końcem",
                isMainWeapon: false,
            },
        ]
    }
];
