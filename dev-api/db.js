const heroes = require('./data/heroes');
const weapons = require('./data/weapons');

module.exports = {
    heroes,
    weapons
};
